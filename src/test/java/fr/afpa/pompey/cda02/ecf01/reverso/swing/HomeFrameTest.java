package fr.afpa.pompey.cda02.ecf01.reverso.swing;


import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

import fr.afpa.pompey.cda02.ecf01.reverso.entity.Client;
import fr.afpa.pompey.cda02.ecf01.reverso.entity.Company;
import fr.afpa.pompey.cda02.ecf01.reverso.entity.Prospect;


public class HomeFrameTest {
    
    @Test
    void filtersTheCompanyList() {
        Client client = new Client();
        Prospect prospect = new Prospect();
        
        // We need to set a businessName as the list will be sorted accordingly
        assertDoesNotThrow( () -> {
            client.setBusinessName("client1");    
            prospect.setBusinessName("prospect1");
        });
        
        Company.getList().add(client);
        Company.getList().add(prospect);
        
        List<Company> filteredList = HomeFrame.getCompanyListOf(Client.class);
        
        assertTrue(filteredList.contains(client));
        assertFalse(filteredList.contains(prospect));
    }
}
