package fr.afpa.pompey.cda02.ecf01.reverso.entity;

import fr.afpa.pompey.cda02.ecf01.reverso.ReversoException;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EmptySource;
import org.junit.jupiter.params.provider.NullSource;
import org.junit.jupiter.params.provider.ValueSource;

public class ProspectTest {
    @ParameterizedTest
    @NullSource
    @EmptySource
    @ValueSource(
        strings = {
            "  ",
            "\t",
            "\n",
            "0452/12/12"
        })
    void invalid_value_should_throw_exception(String invalidValue) {
        Prospect prospect = new Prospect();
        
        assertThrows(
            ReversoException.class,
            () -> { prospect.setProspectionDate(invalidValue); }
        );
    }
    
    @Test
    void correct_value_throws_no_exception() {
        Prospect prospect = new Prospect();
        
        assertDoesNotThrow(() -> { 
            prospect.setProspectionDate("01/02/2002"); });
    }
}
