select
    "id",
    "business_name",
    "domain",
    "address_street_number",
    "address_street_name",
    "address_zip_code",
    "address_city",
    "phone_number",
    "email",
    "comments",
    "prospection_date",
    "interested"
  from "prospect" p
  join "company"
    on p."company_id" = "company"."id"
 where "id" = ?
;
