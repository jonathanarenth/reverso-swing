update "company" set
    "business_name" = ?,
    "domain" = ?,
    "address_street_number" = ?,
    "address_street_name" = ?,
    "address_zip_code" = ?,
    "address_city" = ?,
    "phone_number" = ?,
    "email" = ?,
    "comments" = ?
where "id" = ?
