select
    "id",
    "business_name",
    "domain",
    "address_street_number",
    "address_street_name",
    "address_zip_code",
    "address_city",
    "phone_number",
    "email",
    "comments",
    "turnover",
    "headcount"
  from "client" cli
  join "company" com
    on cli."company_id" = com."id"
;