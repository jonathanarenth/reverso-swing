update "prospect" set
    "prospection_date" = to_date(?,'DD/MM/YYYY'),
    "interested" = ?
where "company_id" = ?
