package fr.afpa.pompey.cda02.ecf01.reverso.persistance.nicerdao;

import fr.afpa.pompey.cda02.ecf01.reverso.ReversoException;
import fr.afpa.pompey.cda02.ecf01.reverso.entity.Company;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URISyntaxException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

public abstract class DAOCompany {
    public static final Logger LOGGER = 
            Logger.getLogger(DAOCompany.class.getName());
    
    public static final Map<String, String> loadedQueryCache = new HashMap<>();
    
    public DAOCompany() {}
    
    protected Connection getConnection() throws SQLException, IOException {
        return ConnectionManager.getConnection();
    }
    
    /**
     * Find a SQL query stored in the 'postgresql' directory and return it
     * 
     * @param name
     * @return
     * @throws IOException
     * @throws URISyntaxException 
     */
    protected String getQuery(String name) 
        throws IOException, URISyntaxException
    {
        // Lookup the query in the cache
        if (! loadedQueryCache.keySet().contains(name)) {
            StringBuilder builder = new StringBuilder();

            // All query files should be in the resource directory, under the 
            // "postgresql" directory and end with the ".sql" extension
            String filePath = "postgresql/" + name + ".sql";

            try (
                BufferedReader reader = 
                        new BufferedReader(
                            new InputStreamReader(this.getClass()
                                .getClassLoader()
                                .getResourceAsStream(filePath)));
            ) {
                reader.lines().forEach(line -> builder.append(line));
            }
            catch (NullPointerException npe) {
                LOGGER.severe(
                        "Could not find the resource file '" + filePath + "'");
            }

            loadedQueryCache.put(name, builder.toString());
            LOGGER.info("Fetch query '"+ name + "' from file: " + loadedQueryCache.get(name));
        }
        else {
            LOGGER.info("Load query '"+ name + "' from cache: ");
        }
        
        return loadedQueryCache.get(name);
    }
    
    protected void setCompanyValueInStatement(
        Company company, 
        PreparedStatement statement
    ) throws SQLException
    {
        statement.setString(1, company.getBusinessName());
        statement.setString(2, company.getDomain().name());
        statement.setString(3, company.getAddressStreetNumber());
        statement.setString(4, company.getAddressStreetName());
        statement.setString(5, company.getAddressZipCode());
        statement.setString(6, company.getAddressCity());
        statement.setString(7, company.getPhoneNumber());
        statement.setString(8, company.getEmail());
        statement.setString(9, company.getComments());
    }
    
    protected PreparedStatement getInsertCompanyStatement(
            Connection connection
    ) throws SQLException, IOException, URISyntaxException
    {
        return connection.prepareStatement(
            this.getQuery("insert_company"),
            PreparedStatement.RETURN_GENERATED_KEYS
        );
    }
    
    protected PreparedStatement getUpdateCompanyStatement(
            Connection connection
    ) throws SQLException, IOException, URISyntaxException
    {
        return connection.prepareStatement(
            this.getQuery("update_company"),
            PreparedStatement.RETURN_GENERATED_KEYS
        );
    }
    
    /**
     * Delete a given Company from the database. Subclasses tables are setup 
     * with a CASCADE rule on deletion, so no further work should be needed
     * 
     * @param Company
     * @return
     * @throws ReversoException 
     */
    public boolean delete(Company company) throws ReversoException {
        boolean success = true;
        
        if (company.getId() == null) {
            throw new UnsupportedOperationException(
                    "The company has no id and should be inserted first");
        }
        
        try (
            PreparedStatement deleteCompanyStatement = 
                    this.getConnection()
                        .prepareStatement(
                            this.getQuery("delete_company"));
        ) {
            int affectedRows;
            
            deleteCompanyStatement.setInt(1, company.getId());
            affectedRows = deleteCompanyStatement.executeUpdate();
            success = affectedRows > 1;
            
            company.setId(null);
        }
        catch (Exception e) {
            throw new ReversoException(e);
        }
        
        return success;
    }
}
