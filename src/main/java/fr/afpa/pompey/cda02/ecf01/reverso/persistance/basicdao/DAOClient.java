package fr.afpa.pompey.cda02.ecf01.reverso.persistance.basicdao;

import fr.afpa.pompey.cda02.ecf01.reverso.ReversoException;
import fr.afpa.pompey.cda02.ecf01.reverso.entity.Client;
import fr.afpa.pompey.cda02.ecf01.reverso.entity.Company;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;


public class DAOClient {
    public static final Logger LOGGER = 
            Logger.getLogger(DAOClient.class.getName());

    public List<Client> findAll() throws ReversoException {
        List<Client> list = new ArrayList<>();
        
        String selectAllUserQuery = "select \n" +
            "    \"id\",\n" +
            "    \"business_name\",\n" +
            "    \"domain\",\n" +
            "    \"address_street_number\",\n" +
            "    \"address_street_name\",\n" +
            "    \"address_zip_code\",\n" +
            "    \"address_city\",\n" +
            "    \"phone_number\",\n" +
            "    \"email\",\n" +
            "    \"comments\",\n" +
            "    \"turnover\",\n" +
            "    \"headcount\"\n" +
            "  from client cli\n" +
            "  join company com\n" +
            "    on cli.company_id = com.id\n"
        ;
        
        try (
            Connection connection = ConnectionManager.getConnection();
            PreparedStatement statement = 
                    connection.prepareStatement(selectAllUserQuery)
        ) {
            ResultSet results = statement.executeQuery();
            
            while (results.next()) {
                Client client = new Client();
                
                client.setId(results.getInt(1));
                client.setBusinessName(results.getString(2));
                client.setDomain(Company.Domain.valueOf(results.getString(3)));
                client.setAddressStreetNumber(results.getString(4));
                client.setAddressStreetName(results.getString(5));
                client.setAddressZipCode(results.getString(6));
                client.setAddressCity(results.getString(7));
                client.setPhoneNumber(results.getString(8));
                client.setEmail(results.getString(9));
                client.setComments(results.getString(10));
                
                client.setTurnover(results.getInt(11));
                client.setHeadcount(results.getInt(12));
                
                list.add(client);
            }
        }
        catch (SQLException sqle) {
            throw new ReversoException(sqle);
        }
        
        return list;
    }
    
    
    
    /**
     * Fetch all available Clients in the database
     * 
     * @return
     * @throws ReversoException 
     */
    public Client find(int id) throws ReversoException {
        Client client = null;

        String selectClientByIdQuery = 
            "select \n" +
            "    \"id\",\n" +
            "    \"business_name\",\n" +
            "    \"domain\",\n" +
            "    \"address_street_number\",\n" +
            "    \"address_street_name\",\n" +
            "    \"address_zip_code\",\n" +
            "    \"address_city\",\n" +
            "    \"phone_number\",\n" +
            "    \"email\",\n" +
            "    \"comments\",\n" +
            "    \"turnover\",\n" +
            "    \"headcount\"\n" +
            "  from \"client\" p\n" +
            "  join \"company\" c\n" +
            "    on p.\"company_id\" = c.\"id\"\n" +
            "  where \"id\" = " + id
        ;
        
        try (
            Connection connection = ConnectionManager.getConnection();
            PreparedStatement statement = 
                    connection.prepareStatement(selectClientByIdQuery)
        ) {
            LOGGER.info("Fetch client by id: " + selectClientByIdQuery);
            ResultSet results = statement.executeQuery();
            
            if (results.next()) {
                client = new Client();
            }
            
            client.setId(results.getInt(1));
            client.setBusinessName(results.getString(2));
            client.setDomain(Company.Domain.valueOf(results.getString(3)));
            client.setAddressStreetNumber(results.getString(4));
            client.setAddressStreetName(results.getString(5));
            client.setAddressZipCode(results.getString(6));
            client.setAddressCity(results.getString(7));
            client.setPhoneNumber(results.getString(8));
            client.setEmail(results.getString(9));
            client.setComments(results.getString(10));

            client.setTurnover(results.getInt(11));
            client.setHeadcount(results.getInt(12));
        }
        catch (SQLException sqle) {
            throw new ReversoException(sqle);
        }
        
        return client;
    }
    
        /**
     * Insert a given client
     * 
     * @param client
     * @return
     * @throws ReversoException 
     */
    public boolean insert(Client client) throws ReversoException {
        if (client.getId() != null) {
            throw new UnsupportedOperationException(
                    "The client already has an id and shouldn't be inserted");
        }
        
        // Unless we detect a problem, we soppose everything went fine
        boolean success = true;
        
        String insertCompanyQuery = 
            "insert into \"company\" \n" +
            "    (\n" +
            "        \"business_name\",\n" +
            "        \"domain\",\n" +
            "        \"address_street_number\",\n" +
            "        \"address_street_name\",\n" +
            "        \"address_zip_code\",\n" +
            "        \"address_city\",\n" +
            "        \"phone_number\",\n" +
            "        \"email\",\n" +
            "        \"comments\"\n" +
            "    )\n" +
            "values\n" +
            "    (\n" +
            "        '" + client.getBusinessName() + "', \n" +
            "        '" + client.getDomain().name() + "', \n" +
            "        " + client.getAddressStreetNumber()+ ", \n" +
            "        '" + client.getAddressStreetName() + "', \n" +
            "        '" + client.getAddressZipCode() + "', \n" +
            "        '" + client.getAddressCity() + "', \n" +
            "        '" + client.getPhoneNumber() + "', \n" +
            "        '" + client.getEmail() + "', \n" +
            "        " + (client.getComments() == null ? "null": "'" + client.getComments()+ "'") + "\n" +
            "    )"
        ;
        
        try (
            Connection connection = ConnectionManager.getConnection();
            PreparedStatement insertCompanyStatement = 
                    connection.prepareStatement(
                        insertCompanyQuery, 
                        PreparedStatement.RETURN_GENERATED_KEYS);

        ) {
            connection.setAutoCommit(false);
            
            LOGGER.info("Insert Company with: " + insertCompanyQuery);
            int affectedRows = insertCompanyStatement.executeUpdate();
            Integer generatedId = null;
            
            ResultSet generatedKeys = insertCompanyStatement.getGeneratedKeys();
            if (!generatedKeys.next() || affectedRows < 1)
            {
                success = false;
            }
            else {
                generatedId = generatedKeys.getInt("id");
            }
            
            client.setId(generatedId);
            
            String insertClientQuery = 
                "insert into \"client\" \n" +
                "    (\n" +
                "        \"company_id\",\n" +
                "        \"turnover\",\n" +
                "        \"headcount\"\n" +
                "    )\n" +
                "values\n" + 
                "    (\n" +
                "        " + client.getId() + ",\n" +
                "        " + client.getTurnover()+ ", \n" +
                "        " + client.getHeadcount()+ "\n" +
                "    )"
            ;
            
            try (PreparedStatement insertClientStatement =
                    connection.prepareStatement(insertClientQuery)
            ) {
                LOGGER.info("Insert Client with: " + insertClientQuery);
                affectedRows = insertClientStatement.executeUpdate();
                
                if (affectedRows < 1) {
                    success = false;
                }
                
                connection.commit();
            }
            finally {
                connection.setAutoCommit(true);
            }
            
            
        }
        catch (SQLException sqle) {
            throw new ReversoException(sqle);
        }
        
        return success;
    }
    
    /**
     * Update a given client
     * 
     * @param client
     * @return
     * @throws ReversoException 
     */
    public boolean update(Client client) throws ReversoException {
        if (client.getId() == null) {
            throw new UnsupportedOperationException(
                    "The client has no id and should be inserted first");
        }
        
        // Unless we detect a problem, we soppose everything went fine
        boolean success = true;
        
        String updateCompanyQuery = 
            "update \"company\"\n" +
            "   set\n" +
            "    \"business_name\"         = '" + client.getBusinessName() + "',\n" +
            "    \"domain\"                = '" + client.getDomain().name() + "',\n" +
            "    \"address_street_number\" = " + client.getAddressStreetNumber()+ ",\n" +
            "    \"address_street_name\"   = '" + client.getAddressStreetName()+ "',\n" +
            "    \"address_zip_code\"      = '" + client.getAddressZipCode()+ "',\n" +
            "    \"address_city\"          = '" + client.getAddressCity()+ "',\n" +
            "    \"phone_number\"          = '" + client.getPhoneNumber()+ "',\n" +
            "    \"email\"                 = '" + client.getEmail() + "',\n" +
            "    \"comments\"              = " + (client.getComments() == null ? "null": "'" + client.getComments()+ "'") + "\n" +
            "where \"id\" = " + client.getId()
        ;
        
        String updateClientQuery = 
            "update \"client\"\n" +
            "   set\n" +
            "    \"turnover\"     = " + client.getTurnover()  + ", \n" +
            "    \"headcount\"     = " + client.getHeadcount() + "\n" +
            "where \"company_id\" = " + client.getId()
        ;
        
        try (
            Connection connection = ConnectionManager.getConnection();
            PreparedStatement updateCompanyStatement = 
                    connection.prepareStatement(updateCompanyQuery);
            PreparedStatement updateClientStatement = 
                connection.prepareStatement(updateClientQuery);
        ) {
            connection.setAutoCommit(false);
            
            LOGGER.info("Update Company with: " + updateCompanyQuery);
            int affectedRows = updateCompanyStatement.executeUpdate();
            
            if (affectedRows < 1) {
                    success = false;
            }
            
            
            LOGGER.info("Update Client with: " + updateClientQuery);
            affectedRows = updateClientStatement.executeUpdate();
            
            if (affectedRows < 1) {
                    success = false;
            }
            
            connection.commit();
            connection.setAutoCommit(true);
        }
        catch (SQLException sqle) {
            throw new ReversoException(sqle);
        }
        
        return success;
    }
    
    /**
     * Insert or Update the given Client
     * 
     * @param client
     * @return
     * @throws ReversoException 
     */
    public boolean save(Client client) throws ReversoException {
        if (client.getId() == null) {
            return this.insert(client);
        }
        else {
            return this.update(client);
        }
    }
    
    /**
     * Delete a given Prospect from the database
     * 
     * @param prospect
     * @return
     * @throws ReversoException 
     */
    public boolean delete(Client client) throws ReversoException {
        boolean success = true;
        
        if (client.getId() == null) {
            throw new UnsupportedOperationException(
                    "The prospect has no id and should be inserted first");
        }
        
        String deleteCompanyQuery 
                = "delete from \"company\" where \"id\" = " + client.getId();
        String deleteProspectQuery 
                = "delete from \"client\" where \"company_id\" = " + client.getId();
        
        try (
            Connection connection = ConnectionManager.getConnection();
            PreparedStatement deleteProspectStatement = 
                connection.prepareStatement(deleteProspectQuery);
            PreparedStatement deleteCompanyStatement = 
                    connection.prepareStatement(deleteCompanyQuery);
        ) {
            connection.setAutoCommit(false);
            
            int affectedRows;
            
            affectedRows = deleteProspectStatement.executeUpdate();
            
            success = affectedRows > 1;
            
            affectedRows = deleteCompanyStatement.executeUpdate();
            
            success = affectedRows > 1;
            
            connection.commit();
            connection.setAutoCommit(true);
            client.setId(null);
        }
        catch (SQLException sqle) {
            throw new ReversoException(sqle);
        }
        
        
        return success;
    }
}
