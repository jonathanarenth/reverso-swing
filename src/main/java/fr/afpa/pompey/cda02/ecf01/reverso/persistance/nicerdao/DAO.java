package fr.afpa.pompey.cda02.ecf01.reverso.persistance.nicerdao;

import fr.afpa.pompey.cda02.ecf01.reverso.ReversoException;
import java.util.List;

public interface DAO<T> {
    public List<T> findAll() throws ReversoException;
    public T find(int id) throws ReversoException;
    public boolean save(T object) throws ReversoException;
    public boolean delete(T object) throws ReversoException;
}
