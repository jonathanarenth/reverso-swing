package fr.afpa.pompey.cda02.ecf01.reverso.persistance.betterdao;


import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;
import java.util.logging.Logger;

public class ConnectionManager {
    private static final Logger LOGGER = 
            Logger.getLogger(ConnectionManager.class.getName());
    
    public static Connection getConnection() throws SQLException {
        Properties databaseProperties = new Properties();
        
        try (InputStream input = 
                fr.afpa.pompey.cda02.ecf01.reverso.persistance.basicdao.ConnectionManager
                        .class
                        .getClassLoader()
                        .getResourceAsStream("database.properties")
        ) {
            databaseProperties.load(input);
        } catch (IOException ieo) {
            LOGGER.severe("database properties file couldn't be loaded:" + ieo.getMessage());
        }
        
        return DriverManager.getConnection(
                // https://jdbc.postgresql.org/documentation/head/connect.html
                (String) databaseProperties.get("url"), databaseProperties
            );
    }
}
