package fr.afpa.pompey.cda02.ecf01.reverso.persistance.basicdao;

import fr.afpa.pompey.cda02.ecf01.reverso.ReversoException;
import fr.afpa.pompey.cda02.ecf01.reverso.entity.Prospect;
import fr.afpa.pompey.cda02.ecf01.reverso.entity.Company;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;


public class DAOProspect {
    public static final Logger LOGGER = 
            Logger.getLogger(DAOProspect.class.getName());
    
    public static final DateFormat frenchDateFormat = 
                                        new SimpleDateFormat("dd/MM/yyyy");
            
    
    /**
     * Fetch all available Prospects in the database
     * 
     * @return
     * @throws ReversoException 
     */
    public List<Prospect> findAll() throws ReversoException {
        List<Prospect> list = new ArrayList<>();
        
        String selectAllProspectQuery = 
            "select \n" +
            "    \"id\",\n" +
            "    \"business_name\",\n" +
            "    \"domain\",\n" +
            "    \"address_street_number\",\n" +
            "    \"address_street_name\",\n" +
            "    \"address_zip_code\",\n" +
            "    \"address_city\",\n" +
            "    \"phone_number\",\n" +
            "    \"email\",\n" +
            "    \"comments\",\n" +
            "    \"prospection_date\",\n" +
            "    \"interested\"\n" +
            "  from prospect p\n" +
            "  join company c\n" +
            "    on p.company_id = c.id\n"
        ;
        
        try (
            Connection connection = ConnectionManager.getConnection();
            PreparedStatement statement = 
                    connection.prepareStatement(selectAllProspectQuery)
        ) {
            LOGGER.info("Fetch all prospect: " + selectAllProspectQuery);
            ResultSet results = statement.executeQuery();
            
            while (results.next()) {
                Prospect prospect = new Prospect();
                
                prospect.setId(results.getInt(1));
                prospect.setBusinessName(results.getString(2));
                prospect.setDomain(Company.Domain.valueOf(results.getString(3)));
                prospect.setAddressStreetNumber(results.getString(4));
                prospect.setAddressStreetName(results.getString(5));
                prospect.setAddressZipCode(results.getString(6));
                prospect.setAddressCity(results.getString(7));
                prospect.setPhoneNumber(results.getString(8));
                prospect.setEmail(results.getString(9));
                prospect.setComments(results.getString(10));
                
                prospect.setProspectionDate(frenchDateFormat.format(results.getDate(11)));
                prospect.setInterested(results.getBoolean(12));
                
                list.add(prospect);
            }
        }
        catch (SQLException sqle) {
            throw new ReversoException(sqle);
        }
        
        return list;
    }
    
    /**
     * Fetch all available Prospects in the database
     * 
     * @return
     * @throws ReversoException 
     */
    public Prospect find(int id) throws ReversoException {
        Prospect prospect = null;

        String selectProspectByIdQuery = 
            "select \n" +
            "    \"id\",\n" +
            "    \"business_name\",\n" +
            "    \"domain\",\n" +
            "    \"address_street_number\",\n" +
            "    \"address_street_name\",\n" +
            "    \"address_zip_code\",\n" +
            "    \"address_city\",\n" +
            "    \"phone_number\",\n" +
            "    \"email\",\n" +
            "    \"comments\",\n" +
            "    \"prospection_date\",\n" +
            "    \"interested\"\n" +
            "  from \"prospect\" p\n" +
            "  join \"company\" c\n" +
            "    on p.\"company_id\" = c.\"id\"\n" +
            "  where \"id\" = " + id
        ;
        
        try (
            Connection connection = ConnectionManager.getConnection();
            PreparedStatement statement = 
                    connection.prepareStatement(selectProspectByIdQuery)
        ) {
            LOGGER.info("Fetch prospect by id: " + selectProspectByIdQuery);
            ResultSet results = statement.executeQuery();
            
            if (results.next()) {
                prospect = new Prospect();
            }
            
            prospect.setId(results.getInt(1));
            prospect.setBusinessName(results.getString(2));
            prospect.setDomain(Company.Domain.valueOf(results.getString(3)));
            prospect.setAddressStreetNumber(results.getString(4));
            prospect.setAddressStreetName(results.getString(5));
            prospect.setAddressZipCode(results.getString(6));
            prospect.setAddressCity(results.getString(7));
            prospect.setPhoneNumber(results.getString(8));
            prospect.setEmail(results.getString(9));
            prospect.setComments(results.getString(10));

            prospect.setProspectionDate(frenchDateFormat.format(results.getDate(11)));
            prospect.setInterested(results.getBoolean(12));
        }
        catch (SQLException sqle) {
            throw new ReversoException(sqle);
        }
        
        return prospect;
    }
    
    /**
     * Insert a given prospect
     * 
     * @param prospect
     * @return
     * @throws ReversoException 
     */
    public boolean insert(Prospect prospect) throws ReversoException {
        if (prospect.getId() != null) {
            throw new UnsupportedOperationException(
                    "The prospect already has an id and shouldn't be inserted");
        }
        
        // Unless we detect a problem, we soppose everything went fine
        boolean success = true;
        
        String insertCompanyQuery = 
            "insert into \"company\" \n" +
            "    (\n" +
            "        \"business_name\",\n" +
            "        \"domain\",\n" +
            "        \"address_street_number\",\n" +
            "        \"address_street_name\",\n" +
            "        \"address_zip_code\",\n" +
            "        \"address_city\",\n" +
            "        \"phone_number\",\n" +
            "        \"email\",\n" +
            "        \"comments\"\n" +
            "    )\n" +
            "values\n" +
            "    (\n" +
            "        '" + prospect.getBusinessName() + "', \n" +
            "        '" + prospect.getDomain().name() + "', \n" +
            "        " + prospect.getAddressStreetNumber()+ ", \n" +
            "        '" + prospect.getAddressStreetName() + "', \n" +
            "        '" + prospect.getAddressZipCode() + "', \n" +
            "        '" + prospect.getAddressCity() + "', \n" +
            "        '" + prospect.getPhoneNumber() + "', \n" +
            "        '" + prospect.getEmail() + "', \n" +
            "        " + (prospect.getComments() == null ? "null": "'" + prospect.getComments()+ "'") + "\n" +
            "    )"
        ;
        
        try (
            Connection connection = ConnectionManager.getConnection();
            PreparedStatement insertCompanyStatement = 
                    connection.prepareStatement(
                        insertCompanyQuery, 
                        PreparedStatement.RETURN_GENERATED_KEYS);

        ) {
            connection.setAutoCommit(false);
            
            LOGGER.info("Insert Company with: " + insertCompanyQuery);
            int affectedRows = insertCompanyStatement.executeUpdate();
            Integer generatedId = null;
            
            ResultSet generatedKeys = insertCompanyStatement.getGeneratedKeys();
            if (!generatedKeys.next() || affectedRows < 1)
            {
                success = false;
            }
            else {
                generatedId = generatedKeys.getInt("id");
            }
            
            prospect.setId(generatedId);
            
            String insertProspectQuery = 
                "insert into \"prospect\" \n" +
                "    (\n" +
                "        \"company_id\",\n" +
                "        \"prospection_date\",\n" +
                "        \"interested\"\n" +
                "    )\n" +
                "values\n" + 
                "    (\n" +
                "        " + prospect.getId() + ",\n" +
                "        to_date('" + prospect.getProspectionDate() + "','DD/MM/YYYY'), \n" +
                "        " + prospect.isInterested() + "\n" +
                "    )"
            ;
            
            try (PreparedStatement insertProspectStatement =
                    connection.prepareStatement(insertProspectQuery)
            ) {
                LOGGER.info("Insert Prospect with: " + insertProspectQuery);
                affectedRows = insertProspectStatement.executeUpdate();
                
                if (affectedRows < 1) {
                    success = false;
                }
                
                connection.commit();
            }
            finally {
                connection.setAutoCommit(true);
            }
            
            
        }
        catch (SQLException sqle) {
            throw new ReversoException(sqle);
        }
        
        return success;
    }
    
    /**
     * Update a given prospect
     * 
     * @param prospect
     * @return
     * @throws ReversoException 
     */
    public boolean update(Prospect prospect) throws ReversoException {
        if (prospect.getId() == null) {
            throw new UnsupportedOperationException(
                    "The prospect has no id and should be inserted first");
        }
        
        // Unless we detect a problem, we soppose everything went fine
        boolean success = true;
        
        String updateCompanyQuery = 
            "update \"company\"\n" +
            "   set\n" +
            "    \"business_name\"         = '" + prospect.getBusinessName() + "',\n" +
            "    \"domain\"                = '" + prospect.getDomain().name() + "',\n" +
            "    \"address_street_number\" = " + prospect.getAddressStreetNumber()+ ",\n" +
            "    \"address_street_name\"   = '" + prospect.getAddressStreetName()+ "',\n" +
            "    \"address_zip_code\"      = '" + prospect.getAddressZipCode()+ "',\n" +
            "    \"address_city\"          = '" + prospect.getAddressCity()+ "',\n" +
            "    \"phone_number\"          = '" + prospect.getPhoneNumber()+ "',\n" +
            "    \"email\"                 = '" + prospect.getEmail() + "',\n" +
            "    \"comments\"              = " + (prospect.getComments() == null ? "null": "'" + prospect.getComments()+ "'") + "\n" +
            "where \"id\" = " + prospect.getId()
        ;
        
        String updateProspectQuery = 
            "update \"prospect\"\n" +
            "   set\n" +
            "    \"prospection_date\"     = to_date('" + prospect.getProspectionDate() + "','DD/MM/YYYY'), \n" +
            "    \"interested\"            = " + prospect.isInterested() + "\n" +
            "where \"company_id\" = " + prospect.getId()
        ;
        
        try (
            Connection connection = ConnectionManager.getConnection();
            PreparedStatement updateCompanyStatement = 
                    connection.prepareStatement(updateCompanyQuery);
            PreparedStatement updateProspectStatement = 
                connection.prepareStatement(updateProspectQuery);
        ) {
            connection.setAutoCommit(false);
            
            LOGGER.info("Update Company with: " + updateCompanyQuery);
            int affectedRows = updateCompanyStatement.executeUpdate();
            
            if (affectedRows < 1) {
                    success = false;
            }
            
            
            LOGGER.info("Update Prospect with: " + updateProspectQuery);
            affectedRows = updateProspectStatement.executeUpdate();
            
            if (affectedRows < 1) {
                    success = false;
            }
            
            connection.commit();
            connection.setAutoCommit(true);
        }
        catch (SQLException sqle) {
            throw new ReversoException(sqle);
        }
        
        return success;
    }
    
    /**
     * Insert or Update the given Prospect
     * 
     * @param prospect
     * @return
     * @throws ReversoException 
     */
    public boolean save(Prospect prospect) throws ReversoException {
        if (prospect.getId() == null) {
            return this.insert(prospect);
        }
        else {
            return this.update(prospect);
        }
    }
    
    /**
     * Delete a given Prospect from the database
     * 
     * @param prospect
     * @return
     * @throws ReversoException 
     */
    public boolean delete(Prospect prospect) throws ReversoException {
        boolean success = true;
        
        if (prospect.getId() == null) {
            throw new UnsupportedOperationException(
                    "The prospect has no id and should be inserted first");
        }
        
        String deleteCompanyQuery 
                = "delete from \"company\" where \"id\" = " + prospect.getId();
        String deleteProspectQuery 
                = "delete from \"prospect\" where \"company_id\" = " + prospect.getId();
        
        try (
            Connection connection = ConnectionManager.getConnection();
            PreparedStatement deleteProspectStatement = 
                connection.prepareStatement(deleteProspectQuery);
            PreparedStatement deleteCompanyStatement = 
                    connection.prepareStatement(deleteCompanyQuery);
        ) {
            connection.setAutoCommit(false);
            
            int affectedRows;
            
            affectedRows = deleteProspectStatement.executeUpdate();
            
            success = affectedRows > 1;
            
            affectedRows = deleteCompanyStatement.executeUpdate();
            
            success = affectedRows > 1;
            
            connection.commit();
            connection.setAutoCommit(true);
            prospect.setId(null);
        }
        catch (SQLException sqle) {
            throw new ReversoException(sqle);
        }
        
        
        return success;
    }
}
