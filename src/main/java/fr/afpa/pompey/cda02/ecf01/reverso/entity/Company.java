package fr.afpa.pompey.cda02.ecf01.reverso.entity;


import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import fr.afpa.pompey.cda02.ecf01.reverso.ReversoException;


public abstract class Company {
    /**
     * Sort companies by business name.
     */
    private static Comparator<Company> sortByBusinessName =
            new Comparator<Company>() {
                @Override
                public int compare(Company o1, Company o2) {
                    return ((Company)o1).getBusinessName()
                            .compareTo(
                            ((Company)o2).getBusinessName());
                }
            };

    private static List<Company> list = new ArrayList<>();

    private static int counter = 1;

    /**
     * Sort the list then return it
     * @return
     */
    public static List<Company> getList() {
        Collections.sort(list, sortByBusinessName);
        return Company.list;
    }


    public enum Domain {
        PRIVATE("Privé"),
        PUBLIC("Public");

        private String representation;

        Domain(String representation) {
            this.setRepresentation(representation);
        }

        public String getRepresentation() {
            return representation;
        }

        public void setRepresentation(String representation) {
            this.representation = representation;
        }
    }

    private Integer id;
    private String businessName;
    private Domain domain = Domain.PUBLIC;
    private String addressStreetNumber;
    private String addressStreetName;
    private String addressZipCode;
    private String addressCity;
    private String phoneNumber;
    private String email;
    private String comments;


    /**
     * A default company constructor, the domain defaults to Domain.PUBLIC
     */
    public Company() {
        super();

        this.id = Company.counter++;
    }

    /**
     * Every value is required except for the comments.
     *
     * @param businessName
     * @param domain
     * @param addressStreetNumber
     * @param addressStreetName
     * @param addressZipCode
     * @param addressCity
     * @param phoneNumber
     * @param email
     * @param comments
     * @throws ReversoException
     */
    public Company(
        final String businessName,
        final Domain domain,
        final String addressStreetNumber,
        final String addressStreetName,
        final String addressZipCode,
        final String addressCity,
        final String phoneNumber,
        final String email,
        final String comments
    ) throws ReversoException
    {
        this.setBusinessName(businessName);
        this.setDomain(domain);
        this.setAddressStreetNumber(addressStreetNumber);
        this.setAddressStreetName(addressStreetName);
        this.setAddressZipCode(addressZipCode);
        this.setAddressCity(addressCity);
        this.setPhoneNumber(phoneNumber);
        this.setEmail(email);
        this.setComments(comments);
    }


    public Integer getId() {
        return id;
    }

    public String getBusinessName() {
        return businessName;
    }

    public Domain getDomain() {
        return domain;
    }

    public String getAddressStreetNumber() {
        return addressStreetNumber;
    }

    public String getAddressStreetName() {
        return addressStreetName;
    }

    public String getAddressZipCode() {
        return addressZipCode;
    }

    public String getAddressCity() {
        return addressCity;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public String getComments() {
        return comments;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }
    /**
     * The business name cannot be null or empty and should contain
     * more than whitespaces.
     *
     * @param businessName
     * @throws fr.afpa.pompey.cda02.ecf01.reverso.ReversoException
     */
    public void setBusinessName(String businessName) throws ReversoException {
        if (businessName == null || businessName.trim().isEmpty()) {
            throw new ReversoException(
                "Business Name cannot be empty (received '" + businessName + "')"
            );
        }

        this.businessName = businessName;
    }

    /**
     * Domain must not be null
     *
     * @param domain
     * @throws ReversoException
     */
    public void setDomain(Domain domain) throws ReversoException {
        if (domain == null) {
            throw new ReversoException("Domain cannot be null");
        }

        this.domain = domain;
    }

    /**
     * The street number cannot be null or empty and should contain
     * more than whitespaces.
     * The String must contain at least one number
     * and can contain letters after the number.
     *
     * @param addressStreetNumber
     * @throws ReversoException
     */
    public void setAddressStreetNumber(String addressStreetNumber)
        throws ReversoException {
        if (addressStreetNumber == null || addressStreetNumber.trim().isEmpty()) {
            throw new ReversoException(
                "addressStreetNumber cannot be empty (received '"
                + addressStreetNumber + "')"
            );
        }
            else if (!addressStreetNumber.matches("[1-9]+[0-9]*[A-Za-z]*+")) {
                throw new ReversoException(
                    "addressStreet number must be start by a number"
                );
            }
        else {
            this.addressStreetNumber = addressStreetNumber;
        }
    }

    /**
     * The business name cannot be null or empty and should contain
     * more than whitespaces.
     *
     * @param addressStreetName
     * @throws ReversoException
     */
    public void setAddressStreetName(String addressStreetName)
        throws ReversoException {

        if (addressStreetName == null || addressStreetName.trim().isEmpty()) {
            throw new ReversoException(
                "Street name cannot be empty (received '"
                + addressStreetName + "'"
            );
        }

        this.addressStreetName = addressStreetName;
    }

    /**
     * The zip code must not be null.
     *
     * @param addressZipCode
     * @throws ReversoException
     */
    public void setAddressZipCode(String addressZipCode)
        throws ReversoException {

        if (addressZipCode == null || addressZipCode.trim().isEmpty()) {
            throw new ReversoException(
                "Zip code cannot be null or empty (received '"
                + addressZipCode + "'"
            );
        }

        this.addressZipCode = addressZipCode;
    }

    /**
     * The business name cannot be null or empty and should contain
     * more than whitespaces.
     *
     * @param addressCity
     * @throws ReversoException
     */
    public void setAddressCity(String addressCity) throws ReversoException {
        if (addressCity == null || addressCity.trim().isEmpty()) {
            throw new ReversoException(
                "The city cannot be null or empty (received '"
                + addressCity + "'"
            );
        }

        this.addressCity = addressCity;
    }

    /**
     * The phone number cannot be null or empty and should contain
     * more than whitespaces.
     * The string should correspond to one of the following format:
     *  - '0123456789'
     *  - '01 23 45 67 89'
     *  - '+001 23 45 67 89'
     *  - '+00 1 23 45 67 89'
     *
     * @param phoneNumber
     * @throws ReversoException
     */
    public void setPhoneNumber(String phoneNumber) throws ReversoException {
        if (phoneNumber == null || phoneNumber.trim().isEmpty()) {
            throw new ReversoException(
                "The phone number cannot be null or empty (received '"
                + phoneNumber + "'"
            );
        }
        if (!phoneNumber.matches(
                "(^[0-9]{10}$)|" +
                "(^[0-9][0-9 .]{12}[0-9]$)|" +
                "(^\\+[0-9]{11}$)|" +
                "(^\\+[0-9][0-9 .]{13,14}[0-9]$)")
        ) {
            throw new ReversoException(
                "The phone number format seems invalid (received '"
                        + phoneNumber
                        + "', expected something of the form "
                        + "XX XX XX XX XX or +XXX XX XX XX XX,"
                        + "with or without whitespaces or dots)"
            );
        }

        this.phoneNumber = phoneNumber;
    }

    /**
     * The email cannot be null, empty nor contain only whitespaces.
     * It should also respect a common format resembling
     * 'xxx.yyy.zzz@sub.example.com'
     *
     * @param email
     * @throws ReversoException
     */
    public void setEmail(String email) throws ReversoException {
        if (email == null || email.trim().isEmpty()) {
            throw new ReversoException(
                "The email cannot be null or empty (received '" + email + "'"
            );
        }
        if (!email.matches("[a-zAZ0-9.]+@[a-zAZ0-9.]+")) {
            throw new ReversoException(
                "The email format seems invalid (received '" + email
                + "', expected something of the form xxx.yyy@example.com)"
            );
        }


        this.email = email;
    }


    /**
     * Comments may be empty or null.
     * If empty, null is stored.
     *
     * @param comments
     */
    public void setComments(String comments) {
        if (comments != null && comments.trim().isEmpty()) {
            comments = null;
        }
        this.comments = comments;
    }

    @Override
    public String toString() {
        return "Company{"
            + "id=" + id
            + ", businessName=" + businessName
            + ", domain=" + domain
            + ", addressStreetNumber=" + addressStreetNumber
            + ", addressStreetName=" + addressStreetName
            + ", addressZipCode=" + addressZipCode
            + ", addressCity=" + addressCity
            + ", phoneNumber=" + phoneNumber
            + ", email=" + email
            + ", comments=" + comments
            + '}';
    }


}
