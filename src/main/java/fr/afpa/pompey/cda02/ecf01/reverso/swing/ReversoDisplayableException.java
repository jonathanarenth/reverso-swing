package fr.afpa.pompey.cda02.ecf01.reverso.swing;

import fr.afpa.pompey.cda02.ecf01.reverso.ReversoException;

/**
 * A special class of exception whose message are set to be displayed
 * to an end-user.
 */
public class ReversoDisplayableException extends ReversoException {
    private Exception originalException;
    
    
    public ReversoDisplayableException(String message) {
        super(message);
    }
    
    public ReversoDisplayableException(
            Exception originalException,
            String message
    ) {
        super(message);
        
        this.originalException = originalException;
    }

    public Exception getOriginalException() {
        return originalException;
    }
    
    public boolean hasOriginalException() {
        return this.originalException != null;
    }
}
