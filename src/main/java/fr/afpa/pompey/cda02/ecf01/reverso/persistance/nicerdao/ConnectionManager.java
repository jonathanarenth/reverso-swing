package fr.afpa.pompey.cda02.ecf01.reverso.persistance.nicerdao;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ConnectionManager {
    private static final Logger LOGGER = 
            Logger.getLogger(ConnectionManager.class.getName());
    private static Connection connection;
    
    static {
        Runtime.getRuntime().addShutdownHook(new Thread() 
        { 
            public void run() 
            { 
                if (ConnectionManager.connection != null) {
                    try {
                        ConnectionManager.connection.close();
                        LOGGER.info("Database closed");
                    } catch (SQLException ex) {
                        LOGGER.severe(ex.getMessage());
                    }
                }
            } 
        }); 
    }
    
    /**
     * Returns an opened Connection instance. 
     * IMPORTANT: Please do not close it yourself
     * 
     * @return
     * @throws SQLException 
     */
    public static Connection getConnection() throws SQLException {
        Properties databaseProperties = new Properties();
        
        if (ConnectionManager.connection == null) {
            try (InputStream input = 
                    fr.afpa.pompey.cda02.ecf01.reverso.persistance.basicdao.ConnectionManager
                            .class
                            .getClassLoader()
                            .getResourceAsStream("database.properties")
            ) {
                databaseProperties.load(input);
            } catch (IOException ieo) {
                LOGGER.severe("database properties file couldn't be loaded:" + ieo.getMessage());
            }
            
            ConnectionManager.connection =
                DriverManager.getConnection(
                // https://jdbc.postgresql.org/documentation/head/connect.html
                (String) databaseProperties.get("url"), databaseProperties
            );
        }
        
        return ConnectionManager.connection;
        
    }
}
