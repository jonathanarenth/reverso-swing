package fr.afpa.pompey.cda02.ecf01.reverso;


public class ReversoException extends Exception {
    private Throwable origin;
    
    public ReversoException(String message) {
        super(message);
    }
    
    public ReversoException(String message, Throwable origin) {
        super(message);
        this.origin = origin;
    }
    
    public ReversoException(Throwable origin) {
        super("Original message: " + origin.getMessage());
        this.origin = origin;
    }
    
    public Throwable getOriginal() {
        return origin;
    }
}
