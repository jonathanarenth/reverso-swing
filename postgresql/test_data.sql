insert into "company" 
    (
        "id",
        "business_name",
        "domain",
        "address_street_number",
        "address_street_name",
        "address_zip_code",
        "address_city",
        "phone_number",
        "email",
        "comments"
    )
values
    (
        1,
        'AFPA Pompey', 
        'PUBLIC', 
        '5', 
        'square Eugène Herzog', 
        '54340', 
        'Frouard', 
        '03 56 98 14 52', 
        'contact@pompey.afpa.fr', 
        null
    ),
    (
        2,
        'Feel Good Inc.', 
        'PRIVATE',
        '151', 
        'Real del Milciades', 
        '33698', 
        'Frouard', 
        '03 89 44 56 89', 
        'info@feelgoodinc.com', 
        'Kilroy was here'
    ),
    (
        3,
        'Skyndu', 
        'PRIVATE',
        '6228', 
        'Anderson street', 
        '25636', 
        'Francisco Villa', 
        '7084417317', 
        'mboothman0@friendfeed.com', 
        null
    ),
    (
        4,
        'Shin R.A.', 
        'PUBLIC',
        '967', 
        'square Eugène Herzog', 
        '54340', 
        'Nkandla', 
        '03 24 42 68 61', 
        'tolynno@bandcamp.com', 
        'No comment'
    ),
    (
        5,
        'Gabcube', 
        'PRIVATE',
        '9', 
        'Riachão das Neves', 
        '36408', 
        'Várzea Grande', 
        '+331 25 72 50 16', 
        'mmitkova@addtoany.com', 
        null
    ),
    (
        6,
        'Jabbersphere', 
        'PRIVATE',
        '87', 
        'rue de Montfermeil', 
        '83308', 
        'Casalinho', 
        '+33 4 87 46 07 57', 
        'bcottah@wufoo.com', 
        'Klaatu barada nikto'
    )
;

alter sequence company_id_seq restart with 8;



insert into "client" 
    ("company_id", "turnover", "headcount")
values
    (1, 250000, 10),
    (2, 7580000, 112),
    (3, 36504500, 220)
;


insert into "prospect"
    ("company_id", "prospection_date", "interested")
values
    (4, '13/09/2019', true),
    (5, '16/09/2016', false),
    (6, '06/05/2018', true)
;
