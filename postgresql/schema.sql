-- Created for PostGreSQL 11

drop table if exists "client";
drop table if exists "prospect";
drop table if exists "company";


create table "company" (
    "id" serial,
    "business_name"         varchar(50) not null,
    "domain"                varchar(10) not null,
    "address_street_number" varchar(10) not null,
    "address_street_name"   varchar(100) not null,
    "address_zip_code"      varchar(10) not null,
    "address_city"          varchar(40) not null,
    "phone_number"          varchar(20) not null,
    "email"                 varchar(50) not null,
    "comments"              text null,

    primary key ("id")
);

create table "client" (
    "company_id"        integer unique,
    "turnover"          integer not null,
    "headcount"         integer not null,

    primary key ("company_id"),
    foreign key ("company_id") references "company"("id") on delete cascade
);

create table "prospect" (
    "company_id"        integer unique,
    "prospection_date"  date not null,
    "interested"        boolean not null,

    primary key ("company_id"),
    foreign key ("company_id") references "company"("id") on delete cascade
);
